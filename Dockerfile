FROM node:17-alpine

WORKDIR /data/app
COPY app/  .

EXPOSE 3000
CMD ["npm", "start"]

